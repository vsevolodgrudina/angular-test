import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HEROES } from '../mock-heroes';
import { HeroService } from '../hero.service'

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  selectedHero: Hero;
  heroes: Hero[];
  hero: Hero = {
    id: 1,
    name: 'Windstorm',
    sureName: 'Hero',
  };

  constructor(private heroService: HeroService) { };

  getHeroes(): void {
    // setTimeout(() => {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
    // }, 1000)
  };

  onSelect(hero: Hero): void {
    if (this.selectedHero === hero) {
      this.selectedHero = null;
    } else {
      this.selectedHero = hero;
    }
  };
  ngOnInit() {
    this.getHeroes();
  };

};
