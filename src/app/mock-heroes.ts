import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Mr. Nice', sureName: 'Hero' },
  { id: 12, name: 'Narco', sureName: 'Hero' },
  { id: 13, name: 'Bombasto', sureName: 'Hero' },
  { id: 14, name: 'Celeritas', sureName: 'Hero' },
  { id: 15, name: 'Magneta', sureName: 'Hero' },
  { id: 16, name: 'RubberMan', sureName: 'Hero' },
  { id: 17, name: 'Dynama', sureName: 'Hero' },
  { id: 18, name: 'Dr IQ', sureName: 'Hero' },
  { id: 19, name: 'Magma', sureName: 'Hero' },
  { id: 20, name: 'Tornado', sureName: 'Hero' }
];