import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
  messages: {
    message: string,
    id: number
  }[] = [];
  constructor() { }
  
  add(message: {message: string, id: number}) {
    this.messages.push(message);
  }
 
  clear(id: number) {
    this.messages = this.messages.filter(el => el.id !== id);
  }
}
