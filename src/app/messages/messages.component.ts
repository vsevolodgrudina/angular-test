import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  showMessage: Boolean = false
  arrayMessages: {message: string, id: number}[] = []

  constructor(
    private messageService: MessageService,
    private route: ActivatedRoute,
  ) { }

  addMessage(message: string) {
    const id = +this.route.snapshot.paramMap.get('id');
    if(id) {
      this.messageService.add({message, id});
    };
  }

  clearMessageForHero() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id) {
      this.messageService.clear(id)
    }
  };

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id) {
      this.arrayMessages = this.messageService.messages.filter(el => el.id === id)
      this.showMessage = true
    }
  };
  
  ngDoCheck() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id) {
      this.arrayMessages = this.messageService.messages.filter(el => el.id === id)
      this.showMessage = true
    }
  };
}
